<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index($filename = "salary-list.csv")
	{
		
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
		// download file in salary-assignment/public directory;
		$file = fopen(FCPATH.$filename, 'w');
		
		$header = array("Month","Salary Date","Bonus Date"); 
		fputcsv($file, $header);
		for($i=1; $i<=12; $i++){
			$tempRowArray = [];
			$year = date("Y");
			$monthMidDate = $year."-".$i."-15";
			// get given date month in textual format
			$currentMonth = date("F", strtotime($monthMidDate));
			// get day of given date
			$dayOfMidMonth = date("D", strtotime($monthMidDate));
			// get last date of selected month
			$monthLastDate = date("Y-m-t", strtotime($monthMidDate));
			$dayOfLastMonth = date("D", strtotime($monthMidDate));
			// Check month last day is weekend then bonus day is upcoming wednesday
			if($dayOfMidMonth == "Sat"){
				$monthMidDate = date("Y-m-d", strtotime($monthMidDate. " +4 day"));
			}
			if($dayOfMidMonth == "Sun"){
				$monthMidDate = date("Y-m-d", strtotime($monthMidDate. " +3 day"));
			}
			// Check month last day is weekend then salary day is upcoming wednesday
			if($dayOfLastMonth == "Sat"){
				$monthLastDate = date("Y-m-d", strtotime($monthLastDate. " +4 day"));
			}
			if($dayOfLastMonth == "Sun"){
				$monthLastDate = date("Y-m-d", strtotime($monthLastDate. " +3 day"));
			}
			$tempRowArray[] =  $currentMonth;
			$tempRowArray[] =  $monthMidDate;
			$tempRowArray[] =  $monthLastDate;
			fputcsv($file,$tempRowArray);
		}
		fclose($file); 
		exit; 
	   
	}

	//--------------------------------------------------------------------

}
